android sdk utilizado:
+ tools:
	- Android SDK Tools : 25.2.5
	- Android SDK Platform-tools : 25.0.3
	- Android SDK Build-tools : 25.0.2

+ Android 6.0 (API 23) //TARGET-VERSION
	- SDK Platform 23 REV 3
	- Google API 23 REV 1
	- Sources for Android SDK 23 REV 1

+ Android 4.1.2 (API 16) //MIN-SDK-VERSION
	- SDK Platform 16 REV 5
	- Google APIS 16 REV 4
	- Sources for Android SDK 16 REV 2

compilacion:

+ npm install -g cordova@6.3.1 (si existe otra version desintalar con npm uninstall -g cordova)
+ npm install -g ionic@2.1.1 (si existe otra version desintalar con npm uninstall -g ionic)
+ npm install cordova-uglify
+ rm -rf node_modules/ platforms/ plugins/
+ npm install
+ bower install
+ ionic state restore
+ ionic build android


Plugin Nativo : Se debe instalar un plugin nativo para incorporar funciones Crypto a la APP 


Para instalar directamente desde un repositorio :

	ionic plugin add http://rundeck3:9080/registrocivil/srcei-security-plugin.git
	
Para instalar el plugin desde una carpeta bajada con anterioridad :

	cordova plugin add srcei_security_plugin --searchpath ../srcei-security-plugin
	

para ejecutar con debuger y log:

	ionic run android -l -c

si no conecta la funcion debuger, ejecutar comando:

	ionic state reset


Dependencias (todas las instaladas via bower)
=======================
	- angular-messages: Se utiliza para controlar los mensajes de validación de los formularios.
	- angular-google-maps: Se utiliza para mostrar el mapa de google y dibujar elementos sobre él.
	- ngCordova": Se utiliza para llamar a las funcionalidades de los plugins de cordova en angular.
	- angular-pdf: Se utiliza para construir el visualizador de PDF. Esta dependencia se basa en pdf.js
	

Salida del comando "ionic plugin list"
=======================
	- call-number 0.0.2 "Cordova Call Number Plugin"
	- cordova-plugin-app-version 0.1.9 "AppVersion"
	- cordova-plugin-compat 1.2.0 "Compat"
	- cordova-plugin-console 1.1.0 "Console"
	- cordova-plugin-crosswalk-webview 2.1.0 "Crosswalk WebView Engine"
	- cordova-plugin-device 1.1.7 "Device"
	- cordova-plugin-file 4.3.3 "File"
	- cordova-plugin-file-opener2 2.0.18 "File Opener2"
	- cordova-plugin-geolocation 2.4.3 "Geolocation"
	- cordova-plugin-inappbrowser 1.5.0 "InAppBrowser"
	- cordova-plugin-network-information 1.3.4 "Network Information"
	- cordova-plugin-splashscreen 4.0.3 "Splashscreen"
	- cordova-plugin-statusbar 2.2.3 "StatusBar"
	- cordova-plugin-whitelist 1.3.3 "Whitelist"
	- cordova-plugin-x-toast 2.6.0 "Toast"
	- cordova-sqlite-storage 2.1.2 "Cordova sqlite storage plugin"
	- ionic-plugin-keyboard 2.2.1 "Keyboard"
	- cordova-plugin-secure-storage

Browser embebido XWalk
=======================
Por razones de compatibilidad, principalmente de Android 4.1.2, se añadió vía cordova el browser Xwalk en su versión 19+.

Generación & Distribución
=======================

A Continuación se resumen los pasos para generar instalables desde la [documentación oficial](https://ionicframework.com/docs/v1/guide/publishing.html)

Android

+ Se creo un certificado de prueba interno de ionix cuyo nombre "IonixSRCeIKeyStore.jks" Passphrase : ionixspa, alias : keyQA / password : ionixqa
+ El certificado se encuentra en owncloud/Registro Civil/release/ 
+ en la carpeta raiz se ejecuta "ionic build --release android"
+ se generarán apks para los distintos flavors y arquitecturas de procesador en la siguiente carpeta "platforms/android/build/outputs/apk/"
+ Firmar un apk unsigned para instalarlo en un dispositivo (NOTA: esto modificara el mismo archivo NO genera uno nuevo, respaldar los unsigned) para ello ejecutamos
+ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ../IonixSRCeIKeyStore.jks android-armv7-release-unsigned.apk keyQA (se completan las password)
+ Comprimir APK con "zipalign -v 4 android-armv7-release-unsigned.apk android-armv7-release-signed-zip.apk"
+ Con un dispositivo Android conectado por USB ejecutar "adb install android-armv7-release-signed-zip.apk"
+ Una vez aprobado este proceso distribuir a clientes los archivos "android-armv7-release-unsigned.apk" y "android-x86-release-unsigned.apk" con el objetivo que ellos repitan el proceso con su certificado productivo y lo publiquen en la tienda


iOS

+ Importar el proyecto a Xcode (ejecutando el archivo {nombreApp}.xcodeprj)
+ Seleccionar Target Civildigital-APP y luego la pestaña Build Settings
     - Buscar el valor 'Runpath Search Paths'
     - Aparecerán los valores Debug y Release, donde Debug esta asociado al valor '@executable_path/Frameworks' mientras que release se encuentra vacío
     - Tanto Debug como Release deben estar asociados al mismo valor, asi que asignamos '@executable_path/Frameworks' al valor asociado a Release
     - Lo anterior es necesario, ya que al correr la aplicación en modo Release o al generar un IPA, es necesario que estos procedimientos encuentren los frameworks o la aplicación se cerrara con un error 'Reason: image not found'
+ Cambiar el número de la versión del proyecto, que corresponda con la que verá el usuario final
+ Logear con la cuenta de desarrollador de apple
+ En la vista "General" del App, seleccionar el Team del registro civil
+ En la barra superior, ir a Product -> Scheme -> Edit Scheme...
+ En la sección "Archive" seleccionar: 
    - Build Configuration: Release
	- Archive Name: Nombre de la aplicación
	- Options: Reveal Archive in Organizer (chequeada)
+ En el mismo recuadro, en la parte superior, presionar donde sale el nombre de un dispositivo iOS (generalmente iPhone 7 Plus), y seleccionar Generic iOS Device.
+ Cerrar el recuadro presionando close
+ En la barra superior, ir a Product -> Clean
+ Luego, en la barra superior, ir a Product -> Archive, el IDE comenzará a "compilar" la aplicación
+ (Este paso sólo se hace si ocurrió un error al momento de hacer Archive) Ir a la sección Build Settings de la App, seleccionar All y bajar hasta el apartado Signing, una vez ahí, en "Code Signing Identity" seleccionar iOS developer, luego hacer Product-> Clean y luego Product -> Archive nuevamente
+ Una vez "compilada" se abrirá una ventana mostrando la build del aplicativo, con su versión correspondiente. Ahora, en el lado derecho se debe seleccionar subir app 
+ Comenzará un proceso de verificación, si lanza error, escoger la opción reset del certificado y luego try again.
+ Una vez la aplicación fue subida a iTunes Connect, conectarse a dicha página, seleccionar el apartado App, y seleccionar la App del registro civil
+ Una vez en el apartado de la App de Registro Civil, escoger "nueva versión"
+ Se debe especificar los cambios que hay en esta versión y otras opciones de personalización respecto a como se ve el aplicativo en el Appstore. Lo importante acá es seleccionar en el apartado Build, la app que se subió a través de Xcode.
+ Una vez está todo seleccionado y modificado, se debe presionar guardar, y luego se manda a la revisión por parte de Apple esperando a que se publique en la AppStore. 
