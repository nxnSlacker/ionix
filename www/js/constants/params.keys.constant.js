angular.module('srcel-frontend')

.constant('PARAMS_KEYS',{
  'CALL_CENTER_NUMBER' : 'call_center_number',
  'FIXED_NUMBER' : 'fixed_number',
  'YOUTUBE_CU' : 'url_youtube_cu',
  'SRCEI_SITE' : 'srcei_site',
  'SOCIAL_FACEBOOK' : 'social_facebook',
  'SOCIAL_YOUTUBE' : 'social_youtube',
  'SOCIAL_TWITTER' : 'social_twitter',
});