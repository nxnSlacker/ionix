angular.module('srcel-frontend')

.constant('PARAMS_DEFAULTS',{
    'CALL_CENTER_NUMBER': '600 370 2000',
    'FIXED_NUMBER': '+56 227061337',
    'SRCEI_SITE': 'http://www.registrocivil.gob.cl',
    'YOUTUBE_CU': 'https://www.youtube.com/watch?v=RvDXyMQET4I',
    'SOCIAL_FACEBOOK': 'https://www.facebook.com/RegistroCivileIdentificacion',
    'SOCIAL_YOUTUBE': 'https://www.youtube.com/user/registrocivilChile',
    'SOCIAL_TWITTER': 'https://www.twitter.com/RegCivil_Chile'
});