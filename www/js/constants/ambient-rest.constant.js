angular.module('srcel-frontend')
.constant('AMBIENT_REST', (function(){
  var ALIVE_URL = '/rest/security/alive';
  var LOGIN_URL = '/rest/security/login';
  var LOGOUT_URL = '/rest/security/logout';
  var ENROLL_URL = '/rest/security/enroll';
  var GET_ALL_URL = '/rest/certificados/listar';
  var GET_URL = '/rest/certificados/traer';
  var GET_NEAREST_OFFICES_URL = '/rest/oficinas/cercanas';
  var GET_ALL_OFFICES_URL = '/rest/oficinas/todas';
  var DOWNLOAD_QR_URL = '/rest/certificados/descargarQR';
  var REQUEST_DOCUMENT_URL = '/rest/certificados/solicitarCertificadoPagado';
  var GET_COUNTRIES_URL = '/rest/parametros/paises';
  var TGR_RETURN_URL = '/RetornoTGR';
  var WEBPAY_RETURN_URL = '/retorno_webpay';
  var GET_PAID_DOCUMENT_URL = '/rest/certificados/obtenerCertificadoPagado';
  var GET_ALL_PARAMETERS_URL = '/rest/parametros/movil';
  return {
    SECURITY: {
      ALIVE: ALIVE_URL,
      LOGIN: LOGIN_URL,
      LOGOUT: LOGOUT_URL,
      ENROLL: ENROLL_URL,
    },
    DOCUMENTS: {
      GET_ALL: GET_ALL_URL,
      GET: GET_URL,
      REQUEST: REQUEST_DOCUMENT_URL,
      DOWNLOAD_QR: DOWNLOAD_QR_URL,
      GET_PAID_DOCUMENT: GET_PAID_DOCUMENT_URL,
      PROCESS_WEBPAY_RESPONSE: '/rest/certificados/procesarRespuestaWebPay'
    },
    OFFICES: {
      GET_NEAREST: GET_NEAREST_OFFICES_URL,
      GET_ALL: GET_ALL_OFFICES_URL
    },
    PARAMS: {
      GET_COUNTRIES: GET_COUNTRIES_URL,
      GET_ALL_PARAMETERS: GET_ALL_PARAMETERS_URL
    },
    PAYMENT: {
      TGR_RETURN: TGR_RETURN_URL,
      WEBPAY_RETURN : WEBPAY_RETURN_URL
    }
  }
})());
