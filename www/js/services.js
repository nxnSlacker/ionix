angular.module('srcel-frontend')
  .service('plantillaService',['$http',function ($http) {

      this.getVersion =  function() {
        return $http.get('/rest/plantilla/version');
      };

  }
]);
