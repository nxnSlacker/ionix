angular.module('srcel-frontend')
.directive('rutValidator', function(Tools) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ngModel) {
        ngModel.$validators.rutValidator = function customValidator(ngModelValue) {
          if(ngModelValue == null)
            return false;
          return Tools.IsRut(ngModelValue);
        }
      }
    };
});
