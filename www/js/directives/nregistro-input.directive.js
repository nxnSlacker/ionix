angular.module('srcel-frontend')
.directive('nRegistroInput', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      var onlyNumbers = function(text) {
        var transformedInput = text.replace(/[^0-9]/g, '');
        ngModelCtrl.$setViewValue(transformedInput);
        ngModelCtrl.$render();
        return transformedInput;
      }
      ngModelCtrl.$parsers.push(onlyNumbers);
    }
  };
});
