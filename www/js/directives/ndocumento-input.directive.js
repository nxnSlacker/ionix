angular.module('srcel-frontend')
.directive('nDocumentoInput', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      var alphaNumeric = function(text) {
        var transformedInput = text.replace(/[^A-Za-z0-9]/g, '');
        ngModelCtrl.$setViewValue(transformedInput);
        ngModelCtrl.$render();
        return transformedInput;
      }
      ngModelCtrl.$parsers.push(alphaNumeric);
    }
  };
});
