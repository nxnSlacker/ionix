angular.module('srcel-frontend')
.directive('rutInput', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      var formatRut = function(text) {
        var transformedInput = text.replace(/[^0-9kK]/g, '').slice(0,9);
        var formatedInput = transformedInput;
        if(transformedInput.length>2)
          formatedInput = transformedInput.substr(0, transformedInput.length-1) + '-' + transformedInput[transformedInput.length-1];
        ngModelCtrl.$setViewValue(formatedInput);
        ngModelCtrl.$render();
        return formatedInput;
      }
      ngModelCtrl.$parsers.push(formatRut);
    }
  };
});
