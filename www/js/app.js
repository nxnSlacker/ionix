// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('srcel-frontend', [
'ionic',
'ngMessages',
'uiGmapgoogle-maps',
'ngCordova',
'pdf',
'srcel-frontend.constants',
'ngAnimate',
'ja.qr',
'ionic-pullup'
])
.run(function($ionicPlatform, $ionicPopup, $rootScope, $window, LABELS, Params, PARAMS_KEYS, PARAMS_DEFAULTS, Session, Version, Security, $state, $stateParams, $cordovaNetwork, Network, $cordovaSplashscreen) {
  $rootScope.LABELS = LABELS;
  $rootScope.PARAMS_DEFAULTS = PARAMS_DEFAULTS;
  $rootScope.PARAMS_KEYS = PARAMS_KEYS;
  $rootScope.Logout = Session.Logout;
  $rootScope.keyboardOpened = false;

  $ionicPlatform.ready(function() {

    //Manejo de X-Agent
    var currentPlatformVersion = ionic.Platform.version();
    var currentPlatformName = ionic.Platform.platform();

    var appData = 'CivilDigitalApp/$OS_NAME/$OS_VERSION/$APP_VERSION_CODE/$APP_VERSION_NAME'

    appData = appData.replace("$OS_NAME", currentPlatformName);
    appData = appData.replace("$OS_VERSION", currentPlatformVersion);

    cordova.getAppVersion.getVersionCode(function(version) {
      console.log('App Version Number : '+version);
      appData = appData.replace("$APP_VERSION_CODE", version);
      cordova.getAppVersion(function(version) {
        console.log('App Version Code : '+version);
        appData = appData.replace("$APP_VERSION_NAME", version);
        localStorage.setItem('X-Agent', appData);
        console.log('X-Agent : '+localStorage.getItem('X-Agent'));     
        
        Security.Check().then(
          function(success){
            console.log('Ok Security Check ');
          },
          function(err){
            console.log('Error security Check ');
          }
        );
        
        Params.GetAllParameters().then(
          function(success){
            console.log('Current Version From Service : '+ success);
    
            Params.GetParameter(PARAMS_KEYS.YOUTUBE_CU, PARAMS_DEFAULTS.YOUTUBE_CU).then(
              function(value){
                console.log('Valor para PARAMS_KEYS.YOUTUBE_CU : ['+value+']');
                $rootScope.noPasswordClickMenu = function(){
                  var ref = cordova.InAppBrowser.open(value, "_system", '');
                }
              },
              function(error){
                console.log('error en GetParameter');
              }
            );
          },
          function(error){
            console.log('error GetAllParameters');
          }
        );

      });
    });

    $cordovaSplashscreen.show();
    setTimeout(function(){
      $cordovaSplashscreen.hide();
    }, 3000);

    $rootScope.isAndroid = ionic.Platform.isAndroid();
    $window.addEventListener('native.keyboardshow', function(){
      document.body.classList.add('keyboard-open');
      $rootScope.$broadcast("keyboardOpen");
    });
    $window.addEventListener('native.keyboardhide', function(){
      $rootScope.$broadcast("keyboardClose");
    });
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.cordova.plugins.StatusBar) {
      StatusBar.style(1);
    }
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      Network.OffNetwork();
    });

    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      Network.OnNetwork();
    });
  });

  $ionicPlatform.on('resume', function() {
    if($cordovaNetwork.isOffline())
    {
      Network.OffNetwork();
    }
  });

  $ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name=="menu"){
      var confirmPopup = $ionicPopup.confirm({
        title: '',
        template: '<center>¿Está seguro que desea salir?</center>',
        okText: 'Aceptar',
        cancelText: 'Cancelar'
      });
      confirmPopup.then(function(res) {
        if(res) {
          navigator.app.exitApp();
        }
     });
    }
    else if($state.current.name=="menu-offices") {
      $state.go('menu');
    }
    else if($state.current.name=="menu-documents") {
      $state.go('menu');
    }
    else if($state.current.name=="menu-document") {
      $state.go('menu-documents');
    }
    else if($state.current.name=="menu-downloads") {
      $state.go('menu');
    }
    else if($state.current.name=="menu-downloads-selection") {
      $state.go('menu-downloads');
    }
    else if($state.current.name=="menu-downloads-selection-request") {
      $state.go('menu-downloads-selection',{group : $stateParams.group});
    }
    else {
      navigator.app.backHistory();
    }
  }, 100);
})
.config(function($ionicConfigProvider, $httpProvider, uiGmapGoogleMapApiProvider, $cordovaInAppBrowserProvider) {
  $ionicConfigProvider.views.swipeBackEnabled(false);
  $ionicConfigProvider.views.forwardCache(false);
  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.backButton.text('');
  $ionicConfigProvider.backButton.icon('ion-android-arrow-back');
  
  var globalDebug = (function () {
    var savedConsole = console;
    return function(debugOn, suppressAll){
        var suppress = suppressAll || false;
        if (debugOn === false) {
            console = {};
            console.log = function () { };
            if(suppress) {
                console.info = function () { };
                console.warn = function () { };
                console.error = function () { };
            } else {
                console.info = savedConsole.info;
                console.warn = savedConsole.warn;
                console.error = savedConsole.error;              
            }
        } else {
            console = savedConsole;
        }
    }
  })();

  globalDebug(false, true);

  uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyCZVvSRRhIgEWVK2ItBS6NllQ6-Vt9jIdA',
      v: '3.24', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization',
      language: 'es'      
  });
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.interceptors.push('httpInterceptor');
  var defaultOptions = {
      location: 'no',
      clearcache: 'no',
      toolbar: 'yes',
      closebuttoncaption:'Cancelar'
    };
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions)
    }, false);
})
