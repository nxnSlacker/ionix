

angular.module('srcel-frontend')
.service('Security', function($rootScope, $q, $injector){
    
    //--- Promesas ----
    var getSecureStorage = function(){
        var deferred = $q.defer();
        
        var ss = new cordova.plugins.SecureStorage(
            function () { 
                console.log('Aviable SecureStorage');
                deferred.resolve(ss);
            },
            function (){
                console.log('Not Aviable SecureStorage');
                deferred.reject();
            },
            'SRCEI'
        );

        return deferred.promise;
    }

    var getKeyFromSecureStorage = function(ss, keyName){
        var deferred = $q.defer();
    
        ss.get(
            function (key) { 
              deferred.resolve(key);
            },
            function (error) { 
                console.log('Error Get Key : ' + error); 
                deferred.reject('Not Aviable Key');
            },
            keyName
          );

        return deferred.promise;
    }

    var removeKeyFromSecureStorage = function(ss, keyName){
        var deferred = $q.defer();
    
        ss.remove(
            function (key) { 
                console.log('Ok Removed ' + key);
                deferred.resolve();
            },
            function (error) { 
                console.log('Error Remove, ' + error);
                deferred.reject('Not Aviable Key for remove');
            },
            keyName
          );

        return deferred.promise;
    }

    var removeAllKeysFromSecureStorage = function(ss){
        var deferred = $q.defer();
    
        removeKeyFromSecureStorage(ss, 'private_key').then( 
            function(success) {
                removeKeyFromSecureStorage(ss, 'public_key').then(
                    function (success){
                        deferred.resolve();
                    },
                    function (error){
                        deferred.reject('Not Aviable Key for remove '+error);
                    }
                )
            },
            function(error){
                deferred.reject('Not Aviable Key for remove '+error);
            }
        )
        return deferred.promise;
    }

    var generateKeyPair = function(ss){
        var deferred = $q.defer();

        window.srcei.generateKeyPair(
            function(keyPair){
                setKeyInSecureStorage(ss, 'private_key', keyPair.privateKey).then( 
                    function(success) {
                        setKeyInSecureStorage(ss, 'public_key', keyPair.publicKey).then( 
                            function(success) {
                                deferred.resolve(keyPair);
                            },
                            function(error){
                                deferred.reject('Not set public key in storage');
                            }
                        )
                    },
                    function(error){
                        deferred.reject('Not set private key in storage');
                    }
                )
            },
            function(error){
                console.log("[PLUGIN] Error en generateKeyPair() "+error);
                deferred.reject('Not generate keyPair');
            }
        );  

        return deferred.promise;
    }


    var setKeyInSecureStorage = function(ss, keyName, keyValue){
        var deferred = $q.defer();
    
        ss.set(
            function (key) { 
                console.log('Ok Set ' + key);
                deferred.resolve();
            },
            function (error) { 
                console.log('Error Set Key, ' + error);
                deferred.reject('Not set key in storage');
            },
            keyName, keyValue
          );

        return deferred.promise;
    }

    var decryptPayload = function(payload, privateKey){
        var deferred = $q.defer();

        window.srcei.decryptPayload( payload, privateKey,
            function(result){
              deferred.resolve(result);
            },
            function(result){
              deferred.reject('Invalid Payload');
            }
        );
        return deferred.promise;
    }

    var enrollApi = function(deviceId, pubKey){
        var deferred = $q.defer();

        var Api = $injector.get('Api');
        
        Api.Security.Enroll(deviceId, pubKey).then(
            function(success){
              deferred.resolve(success);
            },
            function(error){
              deferred.reject(error);
            }
        );

        return deferred.promise;
    }

    var reGenerateKeys = function(){
        console.log('reGenerateKeys() init...');
        var deferred = $q.defer();

        getSecureStorage().then(
            function(ss) {
                generateKeyPair(ss).then( 
                    function (keyPair) {
                        enrollApi(device.uuid, keyPair.publicKey).then(
                            function(response){
                                localStorage.setItem('security_bearer', response.data.jwt);
                                deferred.resolve('Enrolado con exito');
                            },
                            function(error){
                                deferred.reject(error);
                            }
                        )
                    }, function (error){
                        deferred.reject(error);
                    }
                );
            }, function(error){
                deferred.reject(error);
            }
        )

        return deferred.promise;
    }

    var checkSecurity = function(){
        console.log('checkSecurity() init...');
        var deferred = $q.defer();

        securityBearer = localStorage.getItem('security_bearer');

        console.log('securityBearer ? : '+securityBearer);

        if(securityBearer){
            try {
                //Validaremos que securityBearer no este expirado
                var expire = JSON.parse(atob(securityBearer.split('.')[1])).exp;
                console.log('Expire : '+expire);
                var now = Math.round(new Date().getTime() / 1000);
                console.log('Now    : '+now);

                if(expire > now){ 
                    console.log('Token aun valido...');
                    deferred.resolve();
                } else {
                    console.log('El Token caduco, se regenera y enrola!!');
                    reGenerateKeys().then(
                        function(response){
                            deferred.resolve();
                        },
                        function(error){
                            deferred.reject(error);
                        }
                    );
                }
            }catch(err) {
                console.error('Error validando la fecha de expiracion del token Bearer');
                deferred.reject(err);
            } 
        } else{
            reGenerateKeys().then(
                function(response){
                    deferred.resolve();
                },
                function(error){
                    deferred.reject(error);
                }
            );
        }

        return deferred.promise;
    }

    //--- Resolvers onEmits ----

    var onInvalidToken = function(){
        console.log('onInvalidToken function init...');

        //Se limpia localStorage de variables
        localStorage.setItem('security_bearer', '');

        //Se intenta re-enrolar con la llave publica 
        getSecureStorage().then(
            function(ss) {
                getKeyFromSecureStorage(ss, 'public_key').then(
                    function(publicKey) {
                        enrollApi(device.uuid, publicKey).then(
                            function(response){
                                localStorage.setItem('security_bearer', response.data.jwt);
                            },
                            function(error){
                                console.log('Error Enroll Device Api : ' + error); 
                            }
                        );
                    }, 
                    function (error){
                        console.log('Fail get public key ');
                    }
                )
            },
            function (error){
                console.log('Fail open security storage ');
            }
        );
    }

    var onInvalidDecrypt = function (){
        console.log('onInvalidDecrypt function init...');

        localStorage.setItem('security_bearer', '');

        getSecureStorage().then(
            function(ss) {
                removeAllKeysFromSecureStorage(ss).then( 
                    function(success) {
                        generateKeyPair(ss).then( 
                            function(keyPair) {
                                enrollApi(device.uuid, keyPair.publicKey).then(
                                    function(response){
                                        localStorage.setItem('security_bearer', response.data.jwt);
                                    },
                                    function(error){
                                        console.log('Error enroll Device Api : ' + error); 
                                    }
                                );
                            },
                            function(error){
                                console.log('Error Generate KeyPair : ' + error); 
                            }
                        );
                    },
                    function(error){
                        console.log('Error Remove Keys : ' + error); 
                    }
                );
            },
            function (error){
                console.log('Fail open security storage ');
            }
        );

        var Alert = $injector.get('Alert');
        Alert.Error('Petición documento inválida', $rootScope);
    }


  //--- Listeners ----

  $rootScope.$on('invalidToken', function(event) {
    console.log('on invalidToken detected...');
    onInvalidToken();
  });

  $rootScope.$on('invalidDecrypt', function(event) {
    console.log('on invalidDecrypt detected...');
    onInvalidDecrypt();
  });

  //--- Services ---

  return {
    Decrypt: function(payload){
        var deferred = $q.defer();
        console.log('0 : Decrypt payload '+payload);
        getSecureStorage().then(
            function(ss) {
                getKeyFromSecureStorage(ss, 'private_key').then(
                    function(privateKey) {
                        decryptPayload(payload, privateKey).then(
                            function(decryptPayload) {
                                deferred.resolve(decryptPayload);
                            },
                            function (error){
                                console.log('Fail decrypt payload : '+ error);
                                deferred.reject(error);
                            }
                        );
                    }, 
                    function (error){
                        console.log('Fail get private key ');
                        deferred.reject(error);
                    }
                )
            },
            function (error){
                console.log('Fail open security storage ');
                deferred.reject(error);
            }
        );
        return deferred.promise;
    },
    Check : function(){
        var deferred = $q.defer();
        checkSecurity().then(
            function(success){
                deferred.resolve();
            },
            function(error){
                deferred.reject();
            }
        )
        return deferred.promise;
    }
  }
});
