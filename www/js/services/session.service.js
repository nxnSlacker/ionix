angular.module('srcel-frontend')
.service('Session', function(Api, $q, $state, $rootScope, $ionicHistory, $cordovaToast, LABELS){
  var STORAGE_KEYS = {
    TOKEN: 'TOKEN',
    NAME: 'NAME',
    LASTNAME: 'LASTNAME',
    RUT: 'RUT'
  };
  $rootScope.User = {
    Rut: sessionStorage.getItem(STORAGE_KEYS.RUT),
    Name: sessionStorage.getItem(STORAGE_KEYS.NAME),
    LastName: sessionStorage.getItem(STORAGE_KEYS.LASTNAME)
  };
  var ClearSessionData = function(){
    $rootScope.User = {
      Rut: '',
      Name: '',
      LastName: ''
    };
    $ionicHistory.nextViewOptions({
      disableBack: true,
      historyRoot: true
    });
    $state.go('tabs.downloads');
  }
  $rootScope.$on('unauthorized', function(res) {
      $cordovaToast.showLongTop(LABELS.EXPIRED_SESION).then(
        function(success) {
          // success
        },
        function (error) {
          // error
        }
      );
      ClearSessionData();
  });
  $rootScope.$watch(
    function() {
      return $rootScope.User;
    },
    function watchCallback(newValue, oldValue)
    {
      if(newValue == null)
        return;
      sessionStorage.setItem(STORAGE_KEYS.RUT, newValue.Rut);
      sessionStorage.setItem(STORAGE_KEYS.NAME, newValue.Name);
      sessionStorage.setItem(STORAGE_KEYS.LASTNAME, newValue.LastName);
    }
  );
  return {
    IsAlive: function(){
      var deferred = $q.defer();
      Api.Security.Alive().then(
        function(success){
          if(!success.data)
          {
            ClearSessionData();
          }
          deferred.resolve(success.data);
        },
        function(error){

          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    Login: function(user, password){
      var deferred = $q.defer();
      Api.Security.Login(user, password).then(
        function(success){
          if(success.data.exitoLogin)
          {
            $rootScope.User = {
              Rut: success.data.rut,
              Name: success.data.nombre,
              LastName: success.data.apPaterno + ' ' + success.data.apMaterno
            };
          }

          var response  = {
            Status: success.data.exitoLogin,
            ErrorMessages: success.data.errores
          }
          deferred.resolve(response);
        },
        function(error){
          console.error(JSON.stringify(error));
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    Logout: function() {
      ClearSessionData();
      Api.Security.Logout();
    }
  }
})
