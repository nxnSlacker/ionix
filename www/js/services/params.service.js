angular.module('srcel-frontend')
.service('Params', function(Api, $q){
  var Countries = [];
  var PARAM_PREFIX = 'parameter_';

  return {
    GetCountries: function(){
      var deferred = $q.defer();
      if(Countries.length != 0)
      {
        console.log("Paises desde Memoria");
        deferred.resolve(Countries);
        return deferred.promise;
      }
      Api.Params.GetCountries().then(
        function(success){
          console.log("Paises desde API");
          Countries = success.data;
          deferred.resolve(success.data);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    GetAllParameters: function(){
      console.log('GetAllParameters init...');
      var deferred = $q.defer();
      
      var currentVersion = 0;
      
      parametersVersion = localStorage.getItem(PARAM_PREFIX+'_version'); 

      console.log('Storage version : '+parametersVersion);

      if(parametersVersion){
        currentVersion = parametersVersion;
      }

      console.log('Current version : '+currentVersion);

      Api.Params.GetAllParameters(currentVersion).then(
        function(success){
          console.log('Success, version de parameters : '+success.data.version);
          
          if(success.data.version && success.data.version > currentVersion && !angular.equals({}, success.data.parameters)){
            console.log('Vienen nuevos parametros se persisten');
            console.log(JSON.stringify(success.data));
            
            localStorage.setItem(PARAM_PREFIX+'_version', success.data.version);
            angular.forEach(success.data.parameters, function(value, key) {
              localStorage.setItem(PARAM_PREFIX+key, value);
            });

            versionTmp = localStorage.getItem(PARAM_PREFIX+'_version'); 
            console.log('se persistio y quedo : '+versionTmp);

            deferred.resolve(success.data.version);
          } else {
            console.log('Nada nuevo que persistir');
            deferred.resolve(currentVersion);
          }
          return deferred.promise;
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    GetParameter: function(key, defaultValue){
      console.log('GetParameter init...');
      var deferred = $q.defer();
      
      console.log('Key : ['+key+']');
      parameter = localStorage.getItem(PARAM_PREFIX+key); 

      if(parameter){
        console.log('Existe en localStorage : ['+parameter+']');
        deferred.resolve(parameter);  
      } else {        
        if(defaultValue){
          console.log('Se usara default : ['+defaultValue+']');      
          deferred.resolve(defaultValue);  
        }else{
          console.log('Parametro sin valor');
          deferred.reject();
        }
      }
      return deferred.promise;
    }
  }
})
