angular.module('srcel-frontend')
.service('Documents', function(Api, $q, $state, $rootScope, $ionicPlatform, $cordovaFile, $cordovaSQLite, Tools, $cordovaFileOpener2){
  //kilobytes
  var MIN_STORAGE_SPACE_REQUIRED = 20000;
  var INSERT_DOCUMENT_QUERY = "INSERT INTO documents (documentName, documentObject, groupObject) VALUES (?,?,?)";
  var CREATE_TABLE_DOCUMENT_QUERY = "CREATE TABLE IF NOT EXISTS documents (id integer primary key, documentName text, documentObject text, groupObject text)";
  var SELECT_DOCUMENT_QUERY = "SELECT * FROM documents WHERE documentObject = ?";
  var SELECT_DOCUMENTS_QUERY = "SELECT * FROM documents";
  var DELETE_DOCUMENT_QUERY = "DELETE FROM documents WHERE documentObject = ?";
  var db = null;
  var filePath = null;
  var downloadFolder = 'Download';
  var downloadFilePath = '';
  console.log("FILE PATH", downloadFilePath);

  $ionicPlatform.ready(function() {
    console.log("PLATFORM READY DB");
    db = $cordovaSQLite.openDB({ name: "srcel.db", location: 'default'});
    $cordovaSQLite.execute(db, CREATE_TABLE_DOCUMENT_QUERY).then(
      function(success){
        console.log("TABLA CREADA= ", JSON.stringify(success));
      },
      function(error){
        console.log("ERROR AL CREAR TABLA", JSON.stringify(error));
      }
    );

    if(cordova.file){
      downloadFilePath = cordova.file.externalRootDirectory + downloadFolder + '/';
    }
  
    filePath = ionic.Platform.isAndroid()? cordova.file.externalDataDirectory : cordova.file.dataDirectory;
    console.log(filePath);
  });
  var deleteDocument = function(document){
    var deferred = $q.defer();
    $cordovaFile.removeFile(filePath, document.documentName).then(
      function(success)
      {
        $cordovaSQLite.execute(db, DELETE_DOCUMENT_QUERY, [JSON.stringify(document.documentObject)]).then(
          function(res)
          {
            console.log("Documento Eliminado", JSON.stringify(res));
            deferred.resolve(true);
          },
          function(error)
          {
            console.log("Error al eliminar de la base de datos", JSON.stringify(error));
            deferred.reject(error);
          }
        );
      },
      function(error)
      {
        console.log("Error al eliminar el archivo", JSON.stringify(error));
        deferred.reject(error);
      }
    );
    return deferred.promise;
  }
  return {
    GetAll: function(){
      var deferred = $q.defer();
      Api.Documents.GetAll().then(
        function(success){
          deferred.resolve(success);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    Get: function(document){
      var deferred = $q.defer();
      Api.Documents.Get(document).then(
        function(success){
          deferred.resolve(success);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    Request: function(document){
      var deferred = $q.defer();
      Api.Documents.Request(document).then(
        function(success){
          deferred.resolve(success);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    ProcessWebPayResponse: function(tidParam, respcodeParam, fechaSolicitudParam) {
      var deferred = $q.defer();
      Api.Documents.ProcessWebPayResponse(tidParam, respcodeParam, fechaSolicitudParam).then(
        function(success){
          deferred.resolve(success);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    GetPaidDocument: function(requestedDocument){
      var deferred = $q.defer();
      Api.Documents.GetPaidDocument(requestedDocument.idCertificado, requestedDocument.tid, requestedDocument.fechaSolicitudString).then(
        function(success){
          deferred.resolve(success);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    CheckFreeSpace: function(){
      var deferred = $q.defer();
      $cordovaFile.getFreeDiskSpace().then(
        function (space)
        {
          if(space > MIN_STORAGE_SPACE_REQUIRED)
          {
            deferred.resolve();
          }
          else
          {
            deferred.reject();
          }
        },
        function (error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    StoreDocument: function(document, group) {
      var deferred = $q.defer();
      var fileName = Date.now()+'.pdf';
      $cordovaFile.writeFile(filePath, fileName, Tools.Base64ToBinaryArray(document.dataB64), true)
      .then(function (success) {
        console.log(JSON.stringify(group));
        $cordovaSQLite.execute(db, INSERT_DOCUMENT_QUERY, [fileName, JSON.stringify(document), JSON.stringify(group)]).then(
          function(res) {
            deferred.resolve(res);
          },
          function (err)
          {
            console.log("Error de escritura en BD", JSON.stringify(err));
            deferred.reject(err);
          }
        );
      },
      function (error) {
        console.log("Error de escritura en Archivo", JSON.stringify(error));
        deferred.reject(error);
      });
      return deferred.promise;
    },
    DownloadDocument: function(document) {
      var deferred = $q.defer();
      var fileName = Date.now()+'.pdf';
      console.log("DOCUMENT FOR SAVE", JSON.stringify(document));
      $cordovaFile.checkDir(cordova.file.externalRootDirectory, downloadFolder).then(
        function() {
          $cordovaFile.writeFile(downloadFilePath, fileName, Tools.Base64ToBinaryArray(document.dataB64), true)
          .then(function (success) {
            deferred.resolve(downloadFolder+ "/" + fileName);
          },
          function (error) {
            console.log("Error de escritura en Archivo", JSON.stringify(error));
            deferred.reject(error);
          });
        },
        function() {
          $cordovaFile.createDir(cordova.file.externalRootDirectory, downloadFolder, false).then(
            function(success) {
              $cordovaFile.writeFile(downloadFilePath, fileName, Tools.Base64ToBinaryArray(document.dataB64), true)
              .then(function (success) {
                deferred.resolve(downloadFolder + "/" + fileName);
              },
              function (error) {
                console.log("Error de escritura en Archivo", JSON.stringify(error));
                deferred.reject(error);
              });
            },
            function(error) {
              deferred.reject(error);
            }
          );
        }
      );
      return deferred.promise;
    },
    GetStoredDocument: function(document) {
      var deferred = $q.defer();
      $cordovaSQLite.execute(db, SELECT_DOCUMENT_QUERY, [JSON.stringify(document)]).then(
        function(res) {
          if(res.rows.length == 0){
            deferred.reject(null);
          }
          var document = res.rows.item(0);
          document.documentObject = JSON.parse(document.documentObject);
          document.groupObject = JSON.parse(document.groupObject);
          $cordovaFile.readAsArrayBuffer(filePath, document.documentName).then(
            function (data) {
              var blob = new Blob( [data], { type: "application/pdf" });
              //var blob = Tools.NewBlob(data, "application/pdf");
              var url = URL.createObjectURL(blob);
              //var urlCreator = window.URL || window.webkitURL;
              //var url = urlCreator.createObjectURL( blob );
              deferred.resolve({document: document, fileURL: url});
            },
            function (error) {
              console.log("Error al leer archivo", JSON.stringify(error));
              deferred.reject(error);
            }
          );
        },
        function (err)
        {
          console.log("Error al leer de BD", JSON.stringify(err));
          deferred.reject(err);
        }
      );
      return deferred.promise;
    },
    GetStoredDocuments: function() {
      var deferred = $q.defer();
      console.log(JSON.stringify(db), JSON.stringify($rootScope.User.Rut));
      $cordovaSQLite.execute(db, SELECT_DOCUMENTS_QUERY, []).then(
        function(res) {
          if(res.rows.length == 0){
            deferred.resolve([]);
          }
          var documents = [];
          for(var x = 0; x < res.rows.length; x++) {
            var document = res.rows.item(x);
            document.documentObject = JSON.parse(document.documentObject);
            document.groupObject = JSON.parse(document.groupObject);
            documents.push(document);
          }
          deferred.resolve(documents);
        },
        function (err)
        {
          deferred.reject(err);
        }
      );
      return deferred.promise;
    },
    ExportDocument: function(document){
      $cordovaFileOpener2.open(
        filePath + document.documentName,
        'application/pdf'
      ).then(function() {
          // file opened successfully
      }, function(err) {
        console.log('Error Exportación', JSON.stringify(err));
      });
    },
    DeleteDocuments: function(documents){
      if(!Array.isArray(documents))
        documents = [documents];
      var deferreds = [];
      _.forEach(documents, function(document){
        var promise = deleteDocument(document);
        deferreds.push(promise);
      });
      return $q.all(deferreds);
    }
  }
})
