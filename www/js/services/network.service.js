angular.module('srcel-frontend')
.service('Network', function(AMBIENT_REST, $cordovaNetwork, $cordovaToast, LABELS){
  var ShowToast = function(){
    window.plugins.toast.hide(function() {
      $cordovaToast.show(LABELS.NO_NETWORK_CONNECTION, 10000, 'top').then(
        function(args) {
          console.log(args.event);
        },
        function(error) {
          console.error('toast error: ', error);
        }
      );
    });
  }
  return {
    OffNetwork: function() {
      ShowToast();
    },
    OnNetwork: function() {
      console.info("ESTA ONLINE");
      window.plugins.toast.hide();
    }
  }
})
