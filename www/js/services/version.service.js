angular.module('srcel-frontend')
.service('Version', function(Alert, $rootScope, $ionicHistory){
  var STORAGE_KEYS = {
    TOKEN: 'TOKEN',
    NAME: 'NAME',
    LASTNAME: 'LASTNAME',
    RUT: 'RUT'
  };

  var clearSessionData = function(message){
    $rootScope.User = {
      Rut: '',
      Name: '',
      LastName: ''
    };

    $ionicHistory.nextViewOptions({
      disableBack: true,
      historyRoot: true
    });
    
    Alert.InvalidVersion(message, $rootScope);
    
    console.log('clearSessionData !!');
  }

  $rootScope.$on('invalidVersion', function(event, message) {
    console.log('Bad version detect : '+message);
    clearSessionData(message);
  });
})
