angular.module('srcel-frontend')
.service('Offices', function(Api, $q){
  var MAX_OFFICES = 5;
  return {
    GetNearest: function(lat, lng){
      var deferred = $q.defer();
      Api.Offices.GetNearest(lat, lng, MAX_OFFICES).then(
        function(success){
          deferred.resolve(success.data);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
    GetAll: function(){
      var deferred = $q.defer();
      Api.Offices.GetAll().then(
        function(success){
          deferred.resolve(success.data);
        },
        function(error){
          deferred.reject(error);
        }
      );
      return deferred.promise;
    },
  }
})
