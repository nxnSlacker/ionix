angular.module('srcel-frontend')
.service('Api', function(AMBIENT_REST, $http){
  return {
    Security: {
      Alive: function(){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.SECURITY.ALIVE
        });
      },
      Login: function(user, password){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.SECURITY.LOGIN,
          data: {
            rut: user,
            password: password
          }
        });
      },
      Logout: function(){
        return $http({
          method: 'GET',
          url: AMBIENT_REST.SECURITY.LOGOUT
        });
      },
      Enroll: function(deviceId, pubKey){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.SECURITY.ENROLL,
          data: {
            subject: deviceId,
            publicKey: pubKey
          }
        });
      },
    },
    Documents: {
      GetAll: function() {
        return $http({
          method: 'GET',
          url: AMBIENT_REST.DOCUMENTS.GET_ALL
        });
      },
      Get: function(document) {
        return $http({
          method: 'POST',
          url: AMBIENT_REST.DOCUMENTS.GET,
          data: document
        });
      },
      Request: function(document) {
        return $http({
          method: 'POST',
          url: AMBIENT_REST.DOCUMENTS.REQUEST,
          data: document
        });
      },
      ProcessWebPayResponse: function(tidParam, respcodeParam, fechaSolicitudParam) {
        return $http({
          method: 'POST',
          url: AMBIENT_REST.DOCUMENTS.PROCESS_WEBPAY_RESPONSE,
          data: {
            tid: tidParam,
            respcode : respcodeParam,
            fechaSolicitud : fechaSolicitudParam
          }
        });
      },
      GetPaidDocument: function(id, t, reqDate){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.DOCUMENTS.GET_PAID_DOCUMENT,
          data: {
            idCacheCertificado: id,
            tid: t,
            fechaSolicitudString: reqDate
          }
        });
      }
    },
    Offices: {
      GetNearest: function(lat, lng, max){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.OFFICES.GET_NEAREST,
          data: {
            latitud: lat,
            longitud: lng,
             maximo: max
          }
        });
      },
      GetAll: function(){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.OFFICES.GET_ALL,
        });
      }
    },
    Params: {
      GetCountries: function(){
        return $http({
          method: 'GET',
          url: AMBIENT_REST.PARAMS.GET_COUNTRIES,
        });
      },
      GetAllParameters: function(currentVersion){
        return $http({
          method: 'POST',
          url: AMBIENT_REST.PARAMS.GET_ALL_PARAMETERS,
          data:{
            version : currentVersion
          }
        });
      }
    }
  }
})
