angular.module('srcel-frontend')
.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider

  .state('menu-downloads', {
    url: '/downloads',
    templateUrl: './templates/downloads.html',
    controller: 'DownloadsCtrl'
  })
  .state('menu-downloads-selection', {
    url: '/downloads/selection',
    templateUrl: './templates/downloads-selection.html',
    controller: 'DownloadsSelectionCtrl',
    params: {
      group: null
    }
  })
  .state('menu-downloads-selection-request', {
    url: '/downloads/selection/request',
    templateUrl: './templates/request-document.html',
    controller: 'RequestDocumentCtrl',
    params: {
      doc: null,
      group: null
    }
  })
  .state('menu-downloads-selection-request-detail', {
    url: '/downloads/selection/request/detail',
    templateUrl: './templates/request-detail.html',
    controller: 'RequestDetailCtrl',
    params: {
      doc: null,
      requestedDocument: null,
      group: null
    }
  })
  .state('menu-documents', {
    url: '/documents',
    templateUrl: './templates/documents.html',
    controller: 'DocumentsCtrl'
  })
  .state('menu-document', {
    url: 'document',
    templateUrl: './templates/document.html',
    controller: 'DocumentCtrl',
    params: {
      doc: null,
      group: null,
      saved: null
    }
  })
  .state('menu-offices', {
    url: '/offices',
    templateUrl: './templates/offices.html',
    controller: 'OfficesCtrl'
  })

  .state('menu', {
    url: '/menu',
    templateUrl: 'templates/menu.html',
    controller: 'MenuCtrl'
  })
  .state('pricelist', {
    url: '/price-list',
    templateUrl: 'templates/price-list.html',
    controller: 'PriceListCtrl'
  })
  .state('help', {
    url: '/help',
    templateUrl: 'templates/help.html',
    controller: 'HelpCtrl'
  })
  $urlRouterProvider.otherwise('menu');
});
