angular.module('srcel-frontend')
.controller('HelpCtrl', function($scope, $rootScope, LABELS, Params, PARAMS_KEYS, PARAMS_DEFAULTS, $cordovaInAppBrowser) {

  Params.GetParameter(PARAMS_KEYS.CALL_CENTER_NUMBER, PARAMS_DEFAULTS.CALL_CENTER_NUMBER).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.CALL_CENTER_NUMBER : ['+value+']');
      $scope.PHONE_NUMBER = value;
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.FIXED_NUMBER, PARAMS_DEFAULTS.FIXED_NUMBER).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.FIXED_NUMBER : ['+value+']');
      $scope.FIXED_NUMBER = value;
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.SOCIAL_FACEBOOK, PARAMS_DEFAULTS.SOCIAL_FACEBOOK).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.SOCIAL_FACEBOOK : ['+value+']');
      $scope.openFB = function() {
        $cordovaInAppBrowser.open(value, "_system", '');
      };
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.SOCIAL_TWITTER, PARAMS_DEFAULTS.SOCIAL_TWITTER).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.SOCIAL_TWITTER : ['+value+']');
      $scope.openTW = function() {
        $cordovaInAppBrowser.open(value, "_system", '');
      };
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.SOCIAL_YOUTUBE, PARAMS_DEFAULTS.SOCIAL_YOUTUBE).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.SOCIAL_YOUTUBE : ['+value+']');
      $scope.openYT = function() {
        $cordovaInAppBrowser.open(value, "_system", '');
      }
    },
    function(error){
      console.log('error en GetParameter');
    }
  );
});
