angular.module('srcel-frontend')
.controller('DownloadsSelectionCtrl', function($scope, $state, $stateParams, Documents, Params, DocumentsIcons) {
  console.log($stateParams.group)
  $scope.group = $stateParams.group;
  if($scope.group == null)
    $state.go('menu-downloads');

  $scope.nextStep = function(document){
    $state.go('menu-downloads-selection-request', {doc: document});
  }
});
