angular.module('srcel-frontend')
.controller('DocumentCtrl', function($ionicHistory, $cordovaToast, $ionicModal, $ionicActionSheet, $scope, $rootScope, $state, $stateParams, Documents, LABELS, AMBIENT, AMBIENT_REST, $ionicPopover) {
  $scope.document = $stateParams.doc;
  $scope.group = $stateParams.group;
  $scope.storedDocument = [];
  $scope.IsLoading = true;
  $scope.pdfUrl = '';
  $scope.downloadDocument = function(){
    Documents.DownloadDocument($scope.document).then(
      function(filePath){
        $cordovaToast.showShortTop(LABELS.SUCCESS_SAVE + filePath).then(
          function(success) {
            // success
          },
          function (error) {
            // error
          }
        );
      },
      function() {

      }
    );
  }
  $ionicPopover.fromTemplateUrl('templates/partials/document-menu.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $ionicModal.fromTemplateUrl('./templates/qr-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.goToMyDocuments = function() {
    $state.go('menu-documents');
  };

  $scope.deleteDocument = function(){
    console.log("DELETE");
    $scope.popover.hide();
    Documents.DeleteDocuments($scope.storedDocument).then(
      function(success)
      {
        $state.go('menu-documents', {deleted: true});
        $cordovaToast.showLongTop(LABELS.DOCUMENT_DELETED).then(function(success) {
          // success
        }, function (error) {
          // error
        });
      },
      function(error)
      {
        //TODO: QUé sucede si no se puede eliminar el archivo?
      }
    );
  };
  $scope.exportDocument = function() {
    console.log("EXPORT");
    $scope.popover.hide();
    Documents.ExportDocument($scope.storedDocument);
  };
  //Esta función ya no se utiliza porque se pidió remover esta funcionalidad.
  var goToQR = function() {
    var downloadUrl = AMBIENT.BASE_URL + AMBIENT_REST.DOCUMENTS.DOWNLOAD_QR+'?folio='+$scope.storedDocument.documentObject.folio+'&codVerificacion='+$scope.storedDocument.documentObject.codigoVerificacion;
    $scope.qrData = downloadUrl;
    $cordovaToast.showShortTop(LABELS.QR_SHARE_MESSAGE);
    $scope.modal.show();
  };
  $scope.onLoad = function() {
    $scope.IsLoading = false;
  };
  $scope.onError = function(error) {
    console.error("Error PDF load", JSON.stringify(error))
  };
  //Esta función ya no se utiliza porque se pidió remover esta funcionalidad.
  $scope.showOptions = function() {
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: LABELS.EXPORT  + '<i class="icon ion-share"></i>' },
        { text: LABELS.QR_SHARE  + '<i class="icon ion-qr-scanner"></i>' }
      ],
      destructiveText: LABELS.DELETE + '<i class="icon ion-trash-a"></i>',
      titleText: LABELS.OPTIONS,
      cancelText: LABELS.CANCEL,
      cancel: function() {
           // add cancel code..
       },
      buttonClicked: function(index) {
        switch (index) {
          case 0:
            exportDocument();
            break;
          case 1:
            goToQR();
        }
        return true;
      },
      destructiveButtonClicked: function(){
        deleteDocument();
        return true;
      }
    });
  }
  var Init = function() {
    if($scope.document == null){
      $state.go('menu-documents');
      return;
    }
    Documents.GetStoredDocument($scope.document).then(
      function(success){
        console.log("Se encontró el certificado", success);
          $scope.storedDocument = success.document;
          $scope.pdfUrl = success.fileURL; 
          if($stateParams.saved){
            $cordovaToast.showLongTop(LABELS.DOCUMENT_DOWNLOADED).then(function(success) {
              // success
            }, function (error) {
              // error
            });
          }
      },
      function(error){

      }
    );
  };

  Init();

});
