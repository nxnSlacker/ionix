angular.module('srcel-frontend')
.controller('WelcomeCtrl', function($scope, $rootScope, LABELS, $state) {

  $scope.activeIndex = 0;

  var slideChangeEnd = function(swiper) {
    $scope.activeIndex  = swiper.activeIndex;
    console.info($scope.activeIndex);
    $scope.$apply();
  }
  $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
    // data.slider is the instance of Swiper
    $scope.slider = data.slider;
  });

  $scope.sliderOptions = {
    initialSlide: 0,
    direction: 'horizontal',
    speed: 300,
    onSlideChangeEnd: slideChangeEnd,
    autoplay: 500,
    loop: true
  };

  $scope.nextSlide = function(){
    $scope.slider.slideNext();
  }

  $scope.goToApp = function() {
    $state.go('menu');
  }
});
