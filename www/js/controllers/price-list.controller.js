angular.module('srcel-frontend')
.controller('PriceListCtrl', function($scope, $state, PriceList) {
  var temp = _.map(PriceList.GROUPS, function(element) {
       return _.extend({}, element, {shownGroup: false});
  });
  $scope.PriceList = temp;
  $scope.toggleGroup = function(group) {
    group.shownGroup = !group.shownGroup;
  };
});
