angular.module('srcel-frontend')
.controller('RequestDocumentCtrl', function($scope, $rootScope, Params, PARAMS_KEYS, PARAMS_DEFAULTS, $state, $stateParams, Documents, DocumentsParams, $cordovaToast, LABELS, Alert, $cordovaInAppBrowser) {
  $scope.DocumentsParams = DocumentsParams;
  $scope.document = $stateParams.doc;
  $scope.group = $stateParams.group;
  $scope.errorMessages = undefined;
  $scope.keyboardOpened = false;
  $scope.$on("keyboardOpen", function() {
    console.log("OPENED");
    $scope.keyboardOpened = true;
    $scope.$apply();
  });
  $scope.$on("keyboardClose", function() {
    console.log("CLOSED");
    $scope.keyboardOpened = false;
    $scope.$apply();
  });

  if(!$scope.document){
    console.log("Estado documento invalido");
    $scope.isLoading = false;
    $scope.errorMessages = [LABELS.UNKNOWN_ERROR];
    Alert.Error(LABELS.ERROR_REQUEST_DOCUMENT, $scope);

  } else {

    if($scope.document.privado){
      $cordovaToast.showLongTop(LABELS.DOCUMENT_PRIVATE).then(
        function(success) {
          // success
        },
        function (error) {
          // error
        }
      );
      
      Params.GetParameter(PARAMS_KEYS.YOUTUBE_CU, PARAMS_DEFAULTS.YOUTUBE_CU).then(
        function(value){
          console.log('Valor para PARAMS_KEYS.YOUTUBE_CU : ['+value+']');
          $scope.noPasswordClick = function(){
            var ref = cordova.InAppBrowser.open(value, "_system", '');
          }
        },
        function(error){
          console.log('error en GetParameter');
        }
      );

      Params.GetParameter(PARAMS_KEYS.SRCEI_SITE, PARAMS_DEFAULTS.SRCEI_SITE).then(
        function(value){
          console.log('Valor para PARAMS_KEYS.SRCEI_SITE : ['+value+']');
          $scope.srceiSiteClick = function(){
            var ref = cordova.InAppBrowser.open(value, "_system", '');
          }
        },
        function(error){
          console.log('error en GetParameter');
        }
      );


      $scope.sliderOptions = {
        initialSlide: 0,
        direction: 'horizontal',
        speed: 300,
        autoplay: 5000,
        loop: true
      };
    }

    if($scope.document.camposRequeridos.hasOwnProperty(DocumentsParams.PARAMS.PAIS.KEY)){
      Params.GetCountries().then(
        function(countries)
        {
          $scope.countries = countries;
          var defaultCountry = _.find(countries, function(country) { return country.codigo == DocumentsParams.PARAMS.PAIS.DEFAULT_VALUE; }).codigo;
          $scope.document.camposRequeridos[DocumentsParams.PARAMS.PAIS.KEY] = defaultCountry;
        },
        function(){
          //La función error no aplica ya que la ventana anterior los cargó y con esta llamada se traen desde memoria.
        }
      );
    }

    //Fix para campo rut ya que de la base de datos lo envían con puntos.
    if($scope.document.camposRequeridos[DocumentsParams.PARAMS.RUN.KEY] != null){
      $scope.document.camposRequeridos[DocumentsParams.PARAMS.RUN.KEY] = $scope.document.camposRequeridos[DocumentsParams.PARAMS.RUN.KEY].replace(/[.]/g,'');
    }

    $scope.GetDocument = function(form) {
      if(form.$valid && form.$submitted){
        $scope.errorMessages = undefined;
        $scope.isLoading = true;
        Documents.Get($scope.document).then(
          function(data){
            console.log("DATA:   ", JSON.stringify(data));
            Documents.CheckFreeSpace().then(
              function(){
                console.log("HAY ESPACIO");
                var group = {idGrupo: $scope.group.idGrupo, nombre: $scope.group.nombre};
                Documents.StoreDocument(data, group).then(
                  function(success){
                    $scope.isLoading = false;
                    $state.go('menu-document', {doc: data, group: $scope.group, saved: true});
                  },
                  function(error){
                    console.error("ERROR ESCRIBIR:   ", JSON.stringify(error));

                    $scope.isLoading = false;
                    $scope.errorMessages = [LABELS.UNKNOWN_ERROR];
                    Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);
                  }
                );
              },
              function(error){
                console.error("ERROR Espacio:   ", JSON.stringify(error));

                $scope.isLoading = false;
                $scope.errorMessages = [LABELS.NO_DISK_SPACE];
                Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);
              }
            );
          },
          function(error){
            console.error("ERROR:   ", JSON.stringify(error));

            $scope.isLoading = false;
            $scope.errorMessages = [LABELS.CONNECTION_ERROR];
            Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);
          }
        );
      }
    };

    $scope.RequestDocument = function(form){
      if(form.$valid && form.$submitted){
        $scope.errorMessages = undefined;
        $scope.isLoading = true;
        Documents.Request($scope.document).then(
          function(data){
            console.log(JSON.stringify(data));
            if(data.codigoRespuesta == 0){
              $state.go('menu-downloads-selection-request-detail',
                {
                  doc: $scope.document,
                  requestedDocument: data,
                  group: $scope.group
                }
              );
            } else {
              $scope.errorMessages = [data.mensajeError];
              Alert.Error(LABELS.ERROR_REQUEST_DOCUMENT, $scope);
            }
            $scope.isLoading = false;
          },
          function(error){
            console.log(JSON.stringify(error));
            $scope.isLoading = false;
            $scope.errorMessages = [LABELS.UNKNOWN_ERROR];
            Alert.Error(LABELS.ERROR_REQUEST_DOCUMENT, $scope);
          }
        );
      }
    }

  }

});
