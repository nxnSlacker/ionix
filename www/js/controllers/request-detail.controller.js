angular.module('srcel-frontend')
.controller('RequestDetailCtrl', function($scope, $rootScope, $state, $stateParams, Documents, DocumentsParams, LABELS, $cordovaInAppBrowser, AMBIENT_REST, Alert) {
  $scope.DocumentsParams = DocumentsParams;
  $scope.document = $stateParams.doc;
  $scope.group = $stateParams.group;
  $scope.requestedDocument = $stateParams.requestedDocument;
  var regexWebPay=/http[s]?:\/\/.*\/respuestaWebPay\?(respcode=-?\d+&tid=\d+|tid=\d+&respcode=-?\d+).*$/g;
  var retornoTid = null;
  var retornoRespcode = null;
  var retornoPortalPago = false;
  console.log("Documento Pendiente", JSON.stringify($scope.requestedDocument));
  $scope.errorMessages = undefined;

  $scope.$on('$destroy', function(){
    browserLoadStart();
    browserExit();
  });
  var browserLoadStart = $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){
    console.log('response webpay!!!: ', event.url);
    var matches = regexWebPay.exec(event.url); 
    console.log('matches: ', matches[1]);
    if(matches[1] !== undefined) {
      console.log("1");
      retornoPortalPago = true;
      console.log("2");
      retornoTid = /tid=(\d+)/g.exec(matches[1])[1];
      console.log("3");
      console.log('tid: ', retornoTid);
      retornoRespcode = /respcode=-?(\d+)/g.exec(matches[1])[1];
      console.log("4");
      console.log('respcode: ', retornoRespcode);
      $cordovaInAppBrowser.close();
    }
  });
  var browserExit = $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
    if(!retornoPortalPago)
    {
      ErrorPaidAlert();
    }
    else {
      ProcesarRetornoPago();
    }
  });
  $scope.PayDocument = function() {
    $scope.isLoading = true;
    $scope.errorMessages = undefined;
    $cordovaInAppBrowser.open($scope.requestedDocument.urlPago+'?xml='+$scope.requestedDocument.xmlSolicitudPago, "_blank", '');
  }

  var ProcesarRetornoPago = function() {
    $scope.errorMessages = undefined;
    $scope.isLoading = true;
    Documents.ProcessWebPayResponse(retornoTid, retornoRespcode, $scope.requestedDocument.fechaSolicitudString).then(
      function(response) {
        console.log('retornando desde process-webpay-response: ', response);
        if(response.respuesta) {
          GetDocument();
        }
        else{
          ErrorPaidAlert();
        }
      },
      function(error) {
        ErrorPaidAlert();
      }
    );
  }

  var ErrorPaidAlert = function() {
    $scope.isLoading = false;
    $scope.errorMessages = [LABELS.DOCUMENT_NOT_PAID];
    Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);
  }

  var GetDocument = function() {
      $scope.errorMessages = undefined;
      $scope.isLoading = true;
      Documents.GetPaidDocument($scope.requestedDocument).then(
        function(data){
          console.log("DATITOS:   ", JSON.stringify(data));
          Documents.CheckFreeSpace().then(
            function()
            {
              console.log("HAY ESPACIO");
              var group = {idGrupo: $scope.group.idGrupo, nombre: $scope.group.nombre};
              Documents.StoreDocument(data, group).then(
                function(success){
                  $scope.isLoading = false;
                  $state.go('menu-document', {doc: data, group: $scope.group, saved: true});
                },
                function(error){
                  console.log("ERROR ESCRIBIR:   ", JSON.stringify(error));

                  $scope.isLoading = false;
                  $scope.errorMessages = [LABELS.UNKNOWN_ERROR];
                  Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);

                }
              );
            },
            function(error)
            {
              console.log("ERROR Espacio:   ", JSON.stringify(error));

              $scope.isLoading = false;
              $scope.errorMessages = [LABELS.NO_DISK_SPACE];
              Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);

            }
          );
        },
        function(error){
          console.log("ERROR:   ", JSON.stringify(error));

          //TODO: Tomar acción cuando falló el proceso de obtener documento. En este caso en la descarga.
          $scope.isLoading = false;
          $scope.errorMessages = [LABELS.CONNECTION_ERROR];
          Alert.Error(LABELS.ERROR_DOWNLOAD_DOCUMENT, $scope);

        }
      );
  }
});
