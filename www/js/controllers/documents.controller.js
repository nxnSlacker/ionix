angular.module('srcel-frontend')
.controller('DocumentsCtrl', function($scope, $state, $cordovaToast, $stateParams, Documents, LABELS) {
    $scope.selectionMode = false;
    $scope.IsLoading = true;
    $scope.documents = [];
    $scope.turnSelectionMode = function(){
      $scope.selectionMode=!$scope.selectionMode
      _.forEach($scope.documents, function(document){
        document.delete = false;
      });
    }
    $scope.goToDocument = function(document){
      $state.go('menu-document', {doc: document.documentObject, group: document.groupObject});
    }
    $scope.anySelected = function(){
      return _.filter($scope.documents, function(o) { return o.delete == true; }).length > 0;
    }
    $scope.deleteDocuments = function() {
      IsLoading = true;
      $scope.selectionMode=!$scope.selectionMode
      var documentsForDelete = _.filter($scope.documents, function(o) { return o.delete == true; });
      Documents.DeleteDocuments(documentsForDelete).then(
        function(){
          Init();
        },
        function(){
          Init();
        }
      );
    }
    var Init = function() {
      Documents.GetStoredDocuments().then(
        function(success){
          $scope.IsLoading = false;
          $scope.documents = _.map(success, function(element) {
               return _.extend({}, element, {delete: false});
          });
        },
        function(error){
          console.error("Error al cargar mis documentos", JSON.stringify(error))
          $scope.IsLoading = false;
        }
      );
    };
    Init();
});
