angular.module('srcel-frontend')
.controller('DownloadsCtrl', function($scope, $state, Documents, Params, DocumentsIcons) {
  $scope.cantBeLoaded = false;
  $scope.DocumentsGroups = [];
  $scope.FirstLoading = true;

  $scope.GetDocumentsGroups = function (){
    $scope.IsLoading = true;
    Documents.GetAll().then(
      function(documentsGroups)
      {
        Params.GetCountries().then(
          function()
          {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.IsLoading = false;
            $scope.cantBeLoaded = false;
            $scope.FirstLoading = false;
            $scope.DocumentsGroups = _.sortBy(documentsGroups, [function(o) { return o.idGrupo; }]);
          },
          function()
          {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.IsLoading = false;
            $scope.DocumentsGroups = [];
            $scope.FirstLoading = false;
            $scope.cantBeLoaded = true;
          }
        );
      },
      function(error)
      {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.IsLoading = false;
        $scope.DocumentsGroups = [];
        $scope.FirstLoading = false;
        $scope.cantBeLoaded = true;
      }
    );
  };
  $scope.nextStep = function(document){
    $state.go('menu-downloads-request', {doc: document});
  }
  // $scope.toggleGroup = function(group) {
  //   if ($scope.isGroupShown(group)) {
  //     $scope.shownGroup = null;
  //   } else {
  //     $scope.shownGroup = group;
  //   }
  // };
  // $scope.isGroupShown = function(group) {
  //   return $scope.shownGroup === group;
  // };
  $scope.$on('$ionicView.afterEnter', function() {
    $scope.GetDocumentsGroups();
    console.log('AFTER ENTER FIRED');
  });
});
