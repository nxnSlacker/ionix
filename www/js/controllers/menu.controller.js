angular.module('srcel-frontend')
.controller('MenuCtrl', function($scope, $rootScope, LABELS, PARAMS_KEYS, PARAMS_DEFAULTS, $cordovaInAppBrowser, Params) {
  
  Params.GetParameter(PARAMS_KEYS.YOUTUBE_CU, PARAMS_DEFAULTS.YOUTUBE_CU).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.YOUTUBE_CU : ['+value+']');
      $scope.noPasswordClick = function(){
        var ref = cordova.InAppBrowser.open(value, "_system", '');
      }
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.SRCEI_SITE, PARAMS_DEFAULTS.SRCEI_SITE).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.SRCEI_SITE : ['+value+']');
      
      var urlSRCEISiteNoProtocol = value.replace(/^http(s)?\:\/\//i, "");
      $scope.SRCEI_SITE = urlSRCEISiteNoProtocol

      $scope.srceiSiteClick = function(){
        var ref = cordova.InAppBrowser.open(value, "_system", '');
      }
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  Params.GetParameter(PARAMS_KEYS.CALL_CENTER_NUMBER, PARAMS_DEFAULTS.CALL_CENTER_NUMBER).then(
    function(value){
      console.log('Valor para PARAMS_KEYS.CALL_CENTER_NUMBER : ['+value+']');
      $scope.PHONE = LABELS.PHONE.replace('$PHONE', value)
    },
    function(error){
      console.log('error en GetParameter');
    }
  );

  $scope.sliderOptions = {
    initialSlide: 0,
    direction: 'horizontal',
    speed: 300,
    autoplay: 5000,
    loop: true
  };
});