angular.module('srcel-frontend')
.controller('OfficesCtrl', function($scope, $state, $cordovaGeolocation, uiGmapGoogleMapApi, Offices, uiGmapIsReady, LABELS) {
  $scope.IsLoading = true;
  $scope.cantBeLoaded = false;
  $scope.markerOptions = {
    icon: './img/user-marker.png'
  };
  $scope.mapControl = {};
  var posOptions = {
    timeout: 10000,
    enableHighAccuracy: false
  };
  $scope.actions = [
    {
      KEY: '',
      LABEL: LABELS.SELECT_ACTION
    },
    {
      KEY: 'tieneCedula',
      LABEL: LABELS.CED
    },
    // {
    //   KEY: 'tieneCedulaExt',
    //   LABEL: LABELS.CED_EXT
    // },
    {
      KEY: 'tienePasaporte',
      LABEL: LABELS.PASSPORT
    }
  ];
  $scope.changeAction = function(newValue){
    if(newValue == null)
    {
      $scope.filteredOffices = $scope.offices;
    }
    else
      $scope.filteredOffices = _.filter($scope.offices, function(o) { return o[newValue] == true; });
    if(allOfficesLoaded){
      markerCluster.clearMarkers();
      var markers = [];
      var bounds = new google.maps.LatLngBounds();
      var markers = createMarkersFromOffices(bounds);
      $scope.map={
        center: bounds.getCenter(),
        zoom: 13
      };
      setClustersFromMarkers(markers, bounds);
    }
  }
  $scope.currentAction = $scope.actions[0].KEY;
  $scope.selectedOffice = undefined;
  $scope.window = false;
  $scope.showWindow = function(key){
    $scope.selectedOffice = $scope.filteredOffices[key];
    $scope.window = true;
    $scope.$apply();
    console.log(JSON.stringify($scope.selectedOffice));
  };
  $scope.closeWindow = function(){
    this.show = false;
    $scope.window = false;
    $scope.$apply();
  }
  $scope.Init = function()
  {
    return $cordovaGeolocation.getCurrentPosition(posOptions).then(
      function (position)
      {
        $scope.gpsEnabled = true;
        $scope.userPosition = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };
        Offices.GetNearest(parseFloat($scope.userPosition.latitude),parseFloat($scope.userPosition.longitude)).then(
          function(data)
          {
            $scope.offices = _.toArray(data);
            $scope.filteredOffices = $scope.offices;
            uiGmapGoogleMapApi.then(function(maps) {
              var bounds = new google.maps.LatLngBounds();
              bounds.extend(new google.maps.LatLng(parseFloat($scope.userPosition.latitude), parseFloat($scope.userPosition.longitude)));
              _.forEach($scope.offices, function(value, key) {
                bounds.extend(new google.maps.LatLng(parseFloat(value.latitud), parseFloat(value.longitud)));
              });
              $scope.map={
                bounds: bounds
              };
              uiGmapIsReady.promise().then(function (maps) {
                $scope.map = $scope.mapControl.getGMap();
                $scope.map.fitBounds(bounds);
                $scope.IsLoading = false;
                $scope.cantBeLoaded = false;
                $scope.$broadcast('scroll.refreshComplete');
              });
            });
          },
          function(error)
          {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.IsLoading = false;
            $scope.cantBeLoaded = true;
          }
        );

      },
      function(err) {
        allOfficesLoaded = true;
        $scope.gpsEnabled = false;
        Offices.GetAll().then(
          function(data)
          {
            $scope.offices = _.toArray(data);
            $scope.filteredOffices = $scope.offices;
            uiGmapGoogleMapApi.then(function(maps) {
              var bounds = new google.maps.LatLngBounds();
              var markers = createMarkersFromOffices(bounds);
              $scope.map={
                center: bounds.getCenter(),
                zoom: 13
              };
              uiGmapIsReady.promise().then(function (maps) {
                setClustersFromMarkers(markers, bounds);
                $scope.cantBeLoaded = false;
                $scope.$broadcast('scroll.refreshComplete');
              });
            });
          },
          function(error)
          {
            $scope.$broadcast('scroll.refreshComplete');
            $scope.IsLoading = false;
            $scope.cantBeLoaded = true;
          }
        );
      }
    );
  };
  var allOfficesLoaded = false;
  var options = {
      imagePath: 'img/markerclusterer/m'
  };
  var markerCluster;
  var createMarkersFromOffices = function(bounds) {
    var markers = [];
    _.forEach($scope.filteredOffices, function(value, key) {
      bounds.extend(new google.maps.LatLng(parseFloat(value.latitud), parseFloat(value.longitud)));
      var marker = new google.maps.Marker({
          position: new google.maps.LatLng(value.latitud, value.longitud)
      });
      marker.addListener('click', function() {
        $scope.showWindow(key);
      });
      markers.push(marker);
    });
    return markers;
  }
  var setClustersFromMarkers = function(markers, bounds){
    $scope.map = $scope.mapControl.getGMap();
    $scope.map.setCenter(bounds.getCenter());
    $scope.map.fitBounds(bounds);
    $scope.map.setZoom($scope.map.getZoom());
    $scope.IsLoading = false;
    markerCluster = new MarkerClusterer($scope.map, markers, options);
  }

  $scope.posicionActual = function() {
    uiGmapGoogleMapApi.then(function(maps) {
              var bounds = new google.maps.LatLngBounds();
              bounds.extend(new google.maps.LatLng(parseFloat($scope.userPosition.latitude), parseFloat($scope.userPosition.longitude)));
              _.forEach($scope.offices, function(value, key) {
                bounds.extend(new google.maps.LatLng(parseFloat(value.latitud), parseFloat(value.longitud)));
              });
              $scope.map={
                bounds: bounds
              };
              uiGmapIsReady.promise().then(function (maps) {
                $scope.map = $scope.mapControl.getGMap();
                $scope.map.fitBounds(bounds);
                $scope.IsLoading = false;
                $scope.cantBeLoaded = false;
                $scope.$broadcast('scroll.refreshComplete');
              });
            });
  };

  $scope.Init();
});
