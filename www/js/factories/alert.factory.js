angular.module('srcel-frontend')
.factory('Alert', function($rootScope, LABELS, Params, PARAMS_KEYS, PARAMS_DEFAULTS, $ionicPopup, $cordovaInAppBrowser){
  var errorTemplate =
      '<span class="center popup-icon ion-android-alert assertive"> \
      </span> \
      <span class="stable" ng-repeat="errorMessage in errorMessages"> \
        <b>{{errorMessage}}</b> \
      </span> \
      <span class="stable"> \
        {{LABELS.MORE_INFORMATION_CALL}} \
      </span>';

  var invalidVersionTemplate =
      '<span class="center popup-icon ion-android-alert assertive"> </span>\
      <span class="stable"> \
        {{INVALID_VERSION_MESSAGE}} \
      </span>';

  var SRCEI_SITE = '';
  var PHONE_NUMBER = '';

  return {
    Error : function(title, s) {
      Params.GetParameter(PARAMS_KEYS.SRCEI_SITE, PARAMS_DEFAULTS.SRCEI_SITE).then(
        function(value){
          console.log('Alert : Valor para PARAMS_KEYS.SRCEI_SITE : ['+value+']');
          SRCEI_SITE = value;
        },
        function(error){
          console.log('error en GetParameter');
        }
      );

      Params.GetParameter(PARAMS_KEYS.CALL_CENTER_NUMBER, PARAMS_DEFAULTS.CALL_CENTER_NUMBER).then(
        function(value){
          console.log('Alert : Valor para PARAMS_KEYS.CALL_CENTER_NUMBER : ['+value+']');
          PHONE_NUMBER = value;
        },
        function(error){
          console.log('error en GetParameter');
        }
      );
      
      $ionicPopup.show({
        title: title,
        cssClass: 'error-popup custom-popup',
        template: errorTemplate,
        scope: s,
        buttons: [
          {
            text: LABELS.GO_TO_WEBSITE,
            type: 'button-royal',
            onTap: function(e) {
              e.preventDefault();
              $cordovaInAppBrowser.open(SRCEI_SITE, "_system", '');
            }
          },
          {
            text: LABELS.CALL_TO_CALLCENTER,
            type: 'button-balanced',
            onTap: function(e) {
              e.preventDefault();
              window.plugins.CallNumber.callNumber(
                function(){
                  console.info("CALL SUCCESS");
                },
                function(){
                  console.info("CALL FAIL");
                },
              PHONE_NUMBER, false);
            }
          },
          {
            text: LABELS.CLOSE,
            type: 'button-stable'
          }
        ]
      });
    },
    InvalidVersion : function(messageInvalidVersion, myScope) {
      myScope.INVALID_VERSION_MESSAGE = messageInvalidVersion;

      $ionicPopup.show({
        title: LABELS.INVALID_VERSION_TITLE,
        cssClass: 'error-popup custom-popup',
        template: invalidVersionTemplate,
        scope: myScope,
        buttons: [
          {
            text: LABELS.INVALID_VERSION_BUTTON,
            type: 'button-royal',
            onTap: function(e) {
              e.preventDefault();
              var ref = cordova.InAppBrowser.open(ionic.Platform.isAndroid() ? LABELS.INVALID_VERSION_STORE_ANDROID : LABELS.INVALID_VERSION_STORE_IOS, "_system", '');
            }
          },
          {
            text: LABELS.CLOSE,
            type: 'button-stable'
          }
        ]
      });
    }

  };
})
