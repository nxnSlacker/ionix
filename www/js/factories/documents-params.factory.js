angular.module('srcel-frontend')
.factory('DocumentsParams', function() {

  var PARAMS = {
    RUN: {
      KEY: 'RUN',
      NAME: 'RUN',
      EXAMPLE: 'XXXXXXXX-X',
      MAX_LENGTH: '10',
      MIN_LENGTH: '3',
      DIRECTIVE_NAME: 'rut-input',
      VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    PASSWORD: {
      KEY: 'claveunica',
      NAME: 'Clave única',
      EXAMPLE: 'XXXXXX',
      MAX_LENGTH: '100',
      MIN_LENGTH: '6',
      DIRECTIVE_NAME: '',
      VALIDATOR_NAME: '',
      HTML_TYPE: 'password',
      DEFAULT_VALUE: ''
    },
    N_REGISTRO: {
      KEY: 'N Registro',
      NAME: 'Nº de Registro',
      EXAMPLE: 'XXXX',
      MAX_LENGTH: '12',
      MIN_LENGTH: '1',
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    PATENTE: {
      KEY: 'PPU',
      NAME: 'Patente',
      EXAMPLE: 'AA0000',
      MAX_LENGTH: '6',
      MIN_LENGTH: '6',
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    ANO: {
      KEY: 'Año',
      NAME: 'Año',
      EXAMPLE: '2006',
      MAX_LENGTH: 4,
      MIN_LENGTH: 4,
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    N_DOCUMENTO: {
      KEY: 'N Documento',
      NAME: 'Nº de Documento',
      EXAMPLE: 'AA0001111',
      MAX_LENGTH: '10',
      MIN_LENGTH: '1',
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    N_REPERTORIO: {
      KEY: 'N Repertorio',
      NAME: 'Nº de Repertorio',
      EXAMPLE: '3567',
      MAX_LENGTH: '10',
      MIN_LENGTH: '1',
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: 'text',
      DEFAULT_VALUE: ''
    },
    PAIS: {
      KEY: 'Pais',
      NAME: 'País',
      EXAMPLE: 'Chile',
      MAX_LENGTH: '',
      MIN_LENGTH: '',
      //DIRECTIVE_NAME: 'rut-input',
      //VALIDATOR_NAME: 'rut-validator',
      HTML_TYPE: '',
      DEFAULT_VALUE: 86
    },
  };
  return {
    PARAMS: PARAMS
  };
})
