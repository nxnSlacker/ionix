angular.module('srcel-frontend')
.factory('httpInterceptor', function($rootScope, Security, AMBIENT, LABELS, $q){

  return {
    request : function(req) {
      if(req.url.indexOf('/') == 0) {
        req.url = AMBIENT.BASE_URL + req.url;
      }

      req.headers["X-Agent"] = localStorage.getItem('X-Agent');
      req.headers["Authorization"] = 'Bearer '+localStorage.getItem('security_bearer');
      return req;
    },
    response : function(response) {
      if(response.config.url.includes('rest/certificados')){
        return Security.Decrypt(response.data.jwt).then(
          function(decryptPayload){
            try{
              return JSON.parse(decryptPayload);
            }catch(err) {
              $rootScope.$emit("invalidDecrypt");
              return $q.reject(response);
            } 
          },
          function(error){
            $rootScope.$emit("invalidDecrypt");
            return $q.reject(response);
          }
        );
      } else{
        return response;
      }
    },
    responseError: function (response) {
      console.error("ERROR Response : "+JSON.stringify(response));
      
      if (response.status === 401)
      {
          $rootScope.$emit("unauthorized");
      }
      if (response.status === 406)
      {
          $rootScope.$emit("invalidVersion", response && response.data && response.data.message ? response.data.message : LABELS.INVALID_VERSION_MESSAGE);
      }
      if (response.status === 403)
      {
          $rootScope.$emit("invalidToken");
      }
      return $q.reject(response);
    }
  };
})
