angular.module('srcel-frontend')
.factory('DocumentsIcons', function($rootScope) {
  $rootScope.GetIcon = function(groupId)
  {
    return icons[groupId];
  }
  var icons = {
    1: {
      KEY: 1,
      ICON: './img/icon-1.png',
      BACKGROUND: 'background-nacimiento'
    },
    2: {
      KEY: 2,
      ICON: './img/icon-2.png',
      BACKGROUND: 'background-matrimonio'
    },
    3: {
      KEY: 3,
      ICON: './img/icon-3.png',
      BACKGROUND: 'background-defuncion'
    },
    4: {
      KEY: 4,
      ICON: './img/icon-4.png',
      BACKGROUND: 'background-vehiculos'
    },
    5: {
      KEY: 5,
      ICON: './img/icon-6.png',
      BACKGROUND: 'background-antecedentes'
    },
    6: {
      KEY: 6,
      ICON: './img/icon-5.png',
      BACKGROUND: 'background-profesionales'
    },
    7: {
      KEY: 7,
      ICON: './img/icon-8.png',
      BACKGROUND: 'background-discapacidad'
    },
    8: {
      KEY: 8,
      ICON: './img/icon-7.png',
      BACKGROUND: 'background-prendas'
    },
    9: {
      KEY: 9,
      ICON: './img/icon-9.png',
      BACKGROUND: 'background-unioncivil'
    },
    131: {
      KEY: 131,
      ICON: './img/icon-10.png',
      BACKGROUND: 'background-personajuridica'
    },
  };
  return {
    ICONS: icons
  };
})
