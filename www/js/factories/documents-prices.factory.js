angular.module('srcel-frontend')
.factory('PriceList', function($rootScope) {
  var groups = [
    {
      GROUP_NAME: 'Certificados',
      ICON: './img/icon-18.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Nacimiento para matrícula',
          PRICE: '$290'
        },
        {
          NAME: 'Nacimiento para asignación familiar',
          PRICE: '$290'
        },
        {
          NAME: 'Nacimiento para todo trámite',
          PRICE: '$790'
        },
        {
          NAME: 'Matrimonio para asignación familiar',
          PRICE: '$290'
        },
        {
          NAME: 'Matrimonio para todo trámite',
          PRICE: '$710'
        },
        {
          NAME: 'Matrimonio con subinscripciones',
          PRICE: '$1820'
        },
        {
          NAME: 'Defunción para asignación familiar',
          PRICE: '$290'
        },
        {
          NAME: 'Defunción para todo trámite',
          PRICE: '$710'
        },
        {
          NAME: 'Defunción con causa de muerte',
          PRICE: '$710'
        },
        {
          NAME: 'Antecedentes penales',
          PRICE: '$1.050'
        },
        {
          NAME: 'Profesionales',
          PRICE: '$1.050'
        },
        {
          NAME: 'Certificado de Bloqueo de cédula de identidad, pasaporte o licencia de conducir',
          PRICE: '$1.050 c/u'
        },
        {
          NAME: 'Primera filiación',
          PRICE: '$1.300'
        },
        {
          NAME: 'Discapacidad',
          PRICE: '$540'
        },
        {
          NAME: 'Inscripción de vehículo (padrón)',
          PRICE: '$840'
        },
        {
          NAME: 'Anotaciones vigentes de vehículos motorizados',
          PRICE: '$840'
        },
        {
          NAME: 'Multas de tránsito no pagadas',
          PRICE: '$770'
        },
        {
          NAME: 'Hoja de vida del conductor',
          PRICE: '$1.050'
        },
        {
          NAME: 'Informe posesiones efectivas (duplicado)',
          PRICE: '$1.250'
        },
        {
          NAME: 'Histórico de prendas sin desplazamiento',
          PRICE: '$3000'
        },
        {
          NAME: 'Prendas vigentes',
          PRICE: '$3.000'
        },
        {
          NAME: 'Acuerdo de Unión Civil',
          PRICE: '$1.680'
        }
      ]
    },
    {
      GROUP_NAME: 'Cédula de Identidad',
      ICON: './img/icon-11.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Para chilenos',
          PRICE: '$3.820'
        },
        {
          NAME: 'Para extranjeros',
          PRICE: '$4.270'
        }
      ]
    },
    {
      GROUP_NAME: 'Pasaporte',
      ICON: './img/icon-12.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: '32 Páginas',
          PRICE: '$89.660'
        },
        {
          NAME: '64 Páginas',
          PRICE: '$89.740'
        }
      ]
    },
    {
      GROUP_NAME: 'Viaje',
      ICON: './img/icon-13.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Título de viaje para extranjero',
          PRICE: '$2.940'
        },
        {
          NAME: 'Documento de viaje',
          PRICE: '$11.510'
        },
        {
          NAME: 'Salvoconducto Arica - Tacna (*)',
          PRICE: '$1.400'
        }
      ]
    },
    {
      GROUP_NAME: 'Vehículos',
      ICON: './img/icon-14.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Primera inscripción automóviles',
          PRICE: '$48.680'
        },
        {
          NAME: 'Primera inscripción motocicletas',
          PRICE: '$34.340'
        },
        {
          NAME: 'Primera inscripción remolques y semirremolques',
          PRICE: '$38.590'
        },
        {
          NAME: 'Transferencia de vehículos usados (incluye certificado)',
          PRICE: '$21.330'
        },
        {
          NAME: 'Declaración consensual de transferencia ante oficial civil',
          PRICE: '$5.220'
        },
        {
          NAME: 'Anotaciones - Prendas, alzamientos, embargos, cancelaciones y prohibiciones',
          PRICE: '$3.330'
        },
        {
          NAME: 'Anotaciones - Mera tenencia (leasing)',
          PRICE: '$3.330'
        },
        {
          NAME: 'Alzamiento de embargo de Tesorería General de la República',
          PRICE: '$6.660'
        },
        {
          NAME: 'Fotocopia documentos fundandes de la inscripción',
          PRICE: '$680'
        },
        {
          NAME: 'Duplicado placa patente automóviles y remolques',
          PRICE: '$8.100'
        },
        {
          NAME: 'Duplicado placa patente motocicletas',
          PRICE: '$8.100'
        }
      ]
    },
    {
      GROUP_NAME: 'Posesiones Efectivas',
      ICON: './img/icon-15.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Informe de inscripción en el registro nacional de posesiones efectivas',
          PRICE: '$290'
        },
        {
          NAME: 'Informe de inscripción en el registro nacional de testamentos',
          PRICE: '$290'
        },
        {
          NAME: 'Fotocopia de documentos fundantes y solicitud de posesión efectiva',
          PRICE: '$640'
        },
        {
          NAME: 'Arancel de inscripción - Tramo 1 (hasta 15 UTA)',
          PRICE: 'Exento'
        },
        {
          NAME: 'Arancel de inscripción - Tramo 2 (más de 15 UTA y hasta 45 UTA)',
          PRICE: '1,6 UTM'
        },
        {
          NAME: 'Arancel de inscripción - Tramo 3 (más de 45 UTA)',
          PRICE: '2,5 UTM'
        },
        {
          NAME: 'Solicitud de modificación de inventario',
          PRICE: '0,5 UTM'
        },
      ]
    },
    {
      GROUP_NAME: 'Inscripciones',
      ICON: './img/icon-16.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Nacimiento',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Matrimonio - En oficina',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Matrimonio - En domicilio dentro del horario de oficina',
          PRICE: '$21.680'
        },
        {
          NAME: 'Matrimonio - En domicilio fuera del horario de oficina',
          PRICE: '$32.520'
        },
        {
          NAME: 'Matrimonio - Libreta de matrimonio',
          PRICE: '$1.830'
        },
        {
          NAME: 'Matrimonio - Duplicado libreta de matrimonio',
          PRICE: '$2.870'
        },
        {
          NAME: 'Matrimonio - Capitulaciones en el acto de matrimonio',
          PRICE: '$4.510'
        },
        {
          NAME: 'Matrimonio - Capitulaciones antes del acto de matrimonio',
          PRICE: '$4.570'
        },
        {
          NAME: 'Matrimonio - Subinscripción de separación de bienes',
          PRICE: '$4.570'
        },
        {
          NAME: 'Matrimonio - Subinscripción de nulidad de matrimonio',
          PRICE: '$7.710'
        },
        {
          NAME: 'Matrimonio - Subinscripción de divorcio',
          PRICE: '$3.290'
        },
        {
          NAME: 'Matrimonio - Cambio de régimen patrimonial después del matrimonio',
          PRICE: '$1.680'
        },
        {
          NAME: 'Matrimonio - Fotocopia del Acta de Cese de la Convivencia',
          PRICE: '$620'
        },
        {
          NAME: 'Matrimonio - Informe Cese de Convivencia',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Matrimonio - Fotocopia del Acta de Reanudación de la Vida en Común (respecto a separación judicial artículo 27)',
          PRICE: '$620'
        },
        {
          NAME: 'AUC - En oficina',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - En domicilio fuera del horario de oficina',
          PRICE: '$32.520'
        },
        {
          NAME: 'AUC - En domicilio dentro del horario de oficina',
          PRICE: '$21.680'
        },
        {
          NAME: 'AUC - Libreta de AUC',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - Duplicado Libreta de AUC',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - Copia del acta y documentos fundantes',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - Anotación de término de AUC',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - Anotación de escritura pública que sustituya el régimen de comunidad por separación total de bienes',
          PRICE: '$1.680'
        },
        {
          NAME: 'AUC - Inscripción de Acuerdos de Unión Civil o contratos equivalentes celebrados en el extranjero y matrimonios entre personas del mismo sexo celebrados en el extranjero',
          PRICE: '$1.680'
        },
        {
          NAME: 'Defunción',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Copia integral manuscrita de inscripciones, subinscripciones y documentos fundantes de las mismas',
          PRICE: '$1.680'
        },
        {
          NAME: 'Fotocopias de inscripciones',
          PRICE: '$1.970'
        },
        {
          NAME: 'Fotocopia adicional de inscripciones y actas',
          PRICE: '$640'
        },
        {
          NAME: 'Duplicado credencial del registro de la discapacidad',
          PRICE: '$870'
        },
        {
          NAME: 'Profesionales',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Rectificaciones',
          PRICE: 'Gratis'
        }
      ]
    },
    {
      GROUP_NAME: 'Subinscripciones',
      ICON: 'img/icon-17.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Subinscripciones',
          PRICE: '$1.680'
        }
      ]
    },
    {
      GROUP_NAME: 'Beneficios Penales',
      ICON: './img/icon-6.png',
      BACKGROUND: 'background-defuncion',
      DOCUMENTS: [
        {
          NAME: 'Solicitud de beneficios penales',
          PRICE: 'Gratis'
        },
        {
          NAME: 'Eliminación de anotaciones en el registro de conductores',
          PRICE: '$3.310'
        }
      ]
    }
  ];
  return {
    GROUPS: groups
  };
})
