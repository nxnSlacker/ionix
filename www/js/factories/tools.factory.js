angular.module('srcel-frontend')
.factory('Tools', function() {
  return {
    Base64ToBlob: function(base64String){
      base64String = base64String.replace(/\s/g, '');
      var raw = atob(base64String);
      var uint8Array = new Uint8Array(raw.length);
      for (var i = 0; i < raw.length; i++) {
          uint8Array[i] = raw.charCodeAt(i);
      }
      var blob = new Blob( [uint8Array], { type: "application/pdf" });
      return blob;
    },
    Base64ToBinaryArray: function(base64String){
      base64String = base64String.replace(/\s/g, '');
      var raw = atob(base64String);
      var uint8Array = new Uint8Array(raw.length);
      for (var i = 0; i < raw.length; i++) {
          uint8Array[i] = raw.charCodeAt(i);
      }
      var utf8 = new Uint8Array(uint8Array);
      binaryArray = utf8.buffer;
      return binaryArray
    },
    IsRut: function(rut){
      var num = rut.split('-')[0];
      var dv = rut.split('-')[1];
      if(isNaN(num) || num.length == 0 || num.length > 8 || (dv != undefined && (dv.length == 0 || dv.length > 1)))
      {
        return false;
      }
      else
      {
        var newNum = num.toString().split("").reverse().join("");
        for(i=0,j=2,sum=0; i < newNum.length; i++, ((j==7) ? j=2 : j++))
        {
            sum += (parseInt(newNum.charAt(i)) * j);
        }
        var calDV = 11 - (sum % 11);
        calDV = ((calDV == 11) ? 0 : ((calDV == 10) ? "k" : calDV));
        if(String(calDV) == String(dv).toLowerCase())
          return true;
        else {
          return false;
        }
      }
    },
    NewBlob: function(data, datatype)
    {
      var out;

      try {
          out = new Blob([data], {type: datatype});
      }
      catch (e) {
          window.BlobBuilder = window.BlobBuilder ||
                  window.WebKitBlobBuilder ||
                  window.MozBlobBuilder ||
                  window.MSBlobBuilder;

          if (e.name == 'TypeError' && window.BlobBuilder) {
              var bb = new BlobBuilder();
              bb.append(data);
              out = bb.getBlob(datatype);
          }
          else if (e.name == "InvalidStateError") {
              // InvalidStateError (tested on FF13 WinXP)
              out = new Blob([data], {type: datatype});
          }
          else {
              // We're screwed, blob constructor unsupported entirely
          }
      }
      return out;
    }
  };
})
